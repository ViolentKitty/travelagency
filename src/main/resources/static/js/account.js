$(document).ready(function() {
            $("#navBar").load("navBar.html");

            loadUserInfo();
            loadReservationTable();
        });
        function loadUserInfo() {
            $.ajax({
                url: "/userInfo",
                type: "GET",
                }).done(function(data) {
                    $("#userEmail").text("Hello, " + data.loggedEmail);
                    $("#userBalance").text("Balance: " + data.userBalance);
            });
        }
        function loadReservationTable() {
            $("#grid").jsGrid({
                height: "auto",
                width: "100%",

                sorting: true,
                paging: true,
                autoload: true,
                controller: {
                    loadData: function () {
                        var d1 = $.Deferred();
                        $.ajax({
                            type: "get",
                            dataType: "JSON",
                            url: "/account/updateReservations"
                        }).done(function(response) {
                            console.log(response);
                            d1.resolve(response);
                        });
                        return d1.promise();
                    }
                },
                fields: [
                    { name: "passengerId", type: "number", width: 150 },
                    { name: "sourceLocation", title: "Source", type: "text", width: 150 },
                    { name: "destLocation", title: "Destination", type: "text", width: 150 },
                    { name: "flightId", type: "number", width: 150 },
                    { name: "transportMode", type: "number", width: 150 },
                    { name: "groupSize", type: "number", width: 150 }
                ]
            });
        }