package com.bteam.TravelAgency.controllers;

import com.bteam.TravelAgency.repositories.PassengerRepository;
import com.bteam.TravelAgency.repositories.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;

@Controller
public class PaymentController {

    @Autowired
    private PassengerRepository passengerRepository;
    @Autowired
    private PaymentRepository paymentRepository;

    @RequestMapping(path = "/payment", method = RequestMethod.POST)
    public String submitPaymentTransaction(HttpServletResponse response,
                           @RequestParam("cardNumber") String cardNumber,
                           @RequestParam("paymentType") String paymentType) {

        try{
            int num = Integer.parseInt(cardNumber);
        } catch (NumberFormatException e) {
            return "redirect:payment.html";
        }

        String loggedInEmail = SecurityContextHolder.getContext().getAuthentication().getName();

        int success = paymentRepository.savePayment(loggedInEmail, cardNumber, paymentType);

        if (success == 1) {
            passengerRepository.clearUserBalance(loggedInEmail);
        }

        if (success == 1) {
            return "redirect: reservation.html";
        } else {
            return "redirect: payment.html";
        }
    }

}
