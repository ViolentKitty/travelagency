package com.bteam.TravelAgency.controllers;

import com.bteam.TravelAgency.model.Accommodation;
import com.bteam.TravelAgency.model.Flight;
import com.bteam.TravelAgency.model.Location;
import com.bteam.TravelAgency.repositories.AccommodationRepository;
import com.bteam.TravelAgency.repositories.FlightRepository;
import com.bteam.TravelAgency.repositories.PassengerRepository;
import com.bteam.TravelAgency.repositories.ReservationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletResponse;

@Controller
public class ReservationController {
    private static final Logger LOGGER = LoggerFactory.getLogger(SignUpController.class);
    @Autowired
    private ReservationRepository reservationRepository;
    @Autowired
    private FlightRepository flightRepository;
    @Autowired
    private PassengerRepository passengerRepository;
    @Autowired
    private AccommodationRepository accommodationRepository;

    @RequestMapping(path = "/reservation", params = "bookTrip", method = RequestMethod.POST)
    public String bookTrip(HttpServletResponse response,
                            @RequestParam("sourceLocation") String sourceLocation,
                            @RequestParam("destLocation") String destLocation,
                            @RequestParam("mode") String mode,
                            @RequestParam("groupSize") String groupSize,
                            @RequestParam("flightId") String flightId,
                            @RequestParam("accommodationId") String accommodationId,
                            @RequestParam("numDays") String numDays) {


        Location source = reservationRepository.findLocationByCity(sourceLocation.toLowerCase());
        Location destination = reservationRepository.findLocationByCity(destLocation.toLowerCase());

        int accomId, numAccomDays;
        try {
            accomId = Integer.parseInt(accommodationId);
        } catch (Exception e) {
            accomId = -1;
        }
        try {
            numAccomDays = Integer.parseInt(numDays);
        } catch (Exception e) {
            numAccomDays = 0;
        }
        Accommodation accommodation = accommodationRepository.findAccommodationById(accomId);

        // form error checking
        if (source == null || destination == null) {
            System.out.println("Location fields were invalid");
            return "redirect: reservation.html";
        }
        if (source == destination) {
            System.out.println("Source location cannot be same as destination location");
            return "redirect: reservation.html";
        }
        if (Integer.parseInt(groupSize) <= 0 || Integer.parseInt(flightId) <= 0) {
            System.out.println("The type:number inputs must be positive");
            return "redirect: reservation.html";
        }
        Flight flight = flightRepository.findFlightById(Integer.parseInt(flightId));
        if (!(flight.getSourceLocation().equals(source.getCity()) && flight.getDestLocation().equals(destination.getCity()))) {
            System.out.println("You did not pick a valid flight Id");
            return "redirect:reservation.html";
        }
        if (accommodation != null) {
            if (!accommodation.getCityName().equals(destination.getCity())) {
                System.out.println("Accommodation picked is not in the destination city");
                return "redirect:reservation.html";
            }
        }

        String loggedInEmail = SecurityContextHolder.getContext().getAuthentication().getName();

        int success = reservationRepository.saveReservation(loggedInEmail, source, destination, mode, flightId, groupSize);

        int reservationCost = 0;
        if (success == 1) {
            // calculate cost and update user balance accordingly
            reservationCost += (flight.getFare() * Integer.parseInt(groupSize));

            if (accommodation != null) {
                reservationCost += Integer.parseInt(groupSize) * accommodation.getCost() * numAccomDays;
            }
            LOGGER.error("Reservation cost: " + reservationCost);
            success = passengerRepository.updateUserBalance(loggedInEmail, reservationCost);
        }

        if (success == 1) {
            return "redirect: account.html";
        } else {
            return "redirect: reservation.html";
        }
    }
}
