package com.bteam.TravelAgency.controllers;

import com.bteam.TravelAgency.model.Passenger;
import com.bteam.TravelAgency.model.Reservation;
import com.bteam.TravelAgency.repositories.PassengerRepository;
import com.bteam.TravelAgency.repositories.ReservationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class AccountPageController {

    private static final Logger LOGGER = LoggerFactory.getLogger(SignUpController.class);
    @Autowired
    private PassengerRepository passengerRepository;
    @Autowired
    private ReservationRepository reservationRepository;

    @GetMapping("/userInfo")
    public @ResponseBody ResponseEntity<?> updateUserInfo() {

        // get logged-in passenger
        String loggedEmail = SecurityContextHolder.getContext().getAuthentication().getName();
        Passenger passenger = passengerRepository.findByEmailId(loggedEmail);

        // construct json object with user info
        Map<String, Object> output = new HashMap<>(2);
        output.put("loggedEmail", passenger.getFirstName() + " " + passenger.getLastName());
        output.put("userBalance", Integer.toString(passenger.getBalance()));

        return ResponseEntity.status(HttpStatus.OK).body(output);
    }

    @GetMapping("/account/updateReservations")
    public ResponseEntity<?> updateUserReservations() {

        String loggedEmail = SecurityContextHolder.getContext().getAuthentication().getName();
        List<Reservation> results = reservationRepository.findReservationsForUser(loggedEmail);

        return ResponseEntity.status(HttpStatus.OK).body(results);
    }

}
