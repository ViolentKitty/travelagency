package com.bteam.TravelAgency.controllers;

import com.bteam.TravelAgency.model.Accommodation;
import com.bteam.TravelAgency.model.Flight;
import com.bteam.TravelAgency.repositories.AccommodationRepository;
import com.bteam.TravelAgency.repositories.FlightRepository;
import com.bteam.TravelAgency.repositories.ReservationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ReservationOptionsController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ReservationOptionsController.class);

    @Autowired
    private ReservationRepository reservationRepository;
    @Autowired
    private FlightRepository flightRepository;
    @Autowired
    private AccommodationRepository accommodationRepository;

    @GetMapping("/reservation/showOptions")
    public ResponseEntity<?> viewOptions(@RequestParam("sourceLocation") String sourceLocation,
                                         @RequestParam("destLocation") String destLocation,
                                         @RequestParam("option") String option) {

        if (option.equals("flights")) {
            sourceLocation = reservationRepository.locationMapping_nameToId(sourceLocation);
            destLocation = reservationRepository.locationMapping_nameToId(destLocation);

            List<Flight> results = flightRepository.findAllGivenCities(sourceLocation, destLocation);
            LOGGER.error("fligts");
            return ResponseEntity.status(HttpStatus.OK).body(results);
        }
        else if (option.equals("accommodations")) {

            List<Accommodation> results = accommodationRepository.findAccommodationsByCity(destLocation);

            LOGGER.error("accommodations");
            return ResponseEntity.status(HttpStatus.OK).body(results);
        }
        else {
            sourceLocation = reservationRepository.locationMapping_nameToId(sourceLocation);
            destLocation = reservationRepository.locationMapping_nameToId(destLocation);

            List<Flight> results = flightRepository.findAllGivenCities(sourceLocation, destLocation);

            LOGGER.error("something else");
            return ResponseEntity.status(HttpStatus.OK).body(results);
        }
    }

}
