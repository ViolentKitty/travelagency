package com.bteam.TravelAgency.controllers;

import com.bteam.TravelAgency.model.Passenger;
import com.bteam.TravelAgency.repositories.PassengerRepository;
import com.bteam.TravelAgency.services.PassengerService;
import com.bteam.TravelAgency.services.authorization.PassengerDetails;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class SignUpController {
    private static final Logger LOGGER = LoggerFactory.getLogger(SignUpController.class);
    @Autowired
    private PassengerRepository passengerRepository;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private PassengerService passengerService;

    public SignUpController() {}

    @RequestMapping(path = "/signup", method = RequestMethod.GET)
    public String passengerSignupForm() {
        return "signup.html";
    }

    @RequestMapping(path = "/signup", method = RequestMethod.POST)
    public String passengerSignup(Passenger passenger) {

        if(passenger == null){
            LOGGER.error("passengerSignup function returned null");
            return "signup.html";
        }

        passenger.setPassword(passwordEncoder.encode(passenger.getPassword()));
        passenger = passengerService.save(passenger);

        LOGGER.error("Passenger with this information was signed up: " + passenger.toString());

        PassengerDetails passengerDetails = new PassengerDetails(passenger);
        Authentication auth = new UsernamePasswordAuthenticationToken(passengerDetails, passengerDetails.getPassword(), passengerDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(auth);
        System.out.println("About to return from passengerSignupForm()");
        return "redirect:reservation.html";
    }

}

