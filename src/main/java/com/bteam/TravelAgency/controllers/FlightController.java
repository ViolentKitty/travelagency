package com.bteam.TravelAgency.controllers;

import com.bteam.TravelAgency.model.Flight;
import com.bteam.TravelAgency.repositories.FlightRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/flights")
public class FlightController {

    @Autowired
    private FlightRepository flightRepository;

    @GetMapping("/all")
    public ResponseEntity<?> showAll() {

        List<Flight> results = flightRepository.findAll();
        return ResponseEntity.status(HttpStatus.OK).body(results);
    }

}
