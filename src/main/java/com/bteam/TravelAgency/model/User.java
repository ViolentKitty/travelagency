package com.bteam.TravelAgency.model;

public abstract class User {

    private int id;
    private String emailId;
    private String password;

    public User() {}

    public User(User user) {
        this.id = user.getId();
        this.emailId = user.getEmailId();
        this.password = user.getPassword();
    }

    public int getId() {
        return id;
    }

    public User setId(int id) {
        this.id = id;
        return this;
    }

    public String getEmailId() {
        return emailId;
    }

    public User setEmailId(String emailId) {
        this.emailId = emailId;
        return this;
    }

    public String getPassword() {
        return password;
    }

    public User setPassword(String password) {
        this.password = password;
        return this;
    }

}
