package com.bteam.TravelAgency.model;

public class Location {

    private int locationId;
    private String city;
    private String country;

    public Location() {}

    public int getLocationId() { return locationId; }

    public Location setLocationId(int locationId) {
        this.locationId = locationId;
        return this;
    }

    public String getCity() { return city; }

    public Location setCity(String city) {
        this.city = city;
        return this;
    }

    public String getCountry() { return country; }

    public Location setCountry(String country) {
        this.country = country;
        return this;
    }

    public String toString() {
        return "Location toString():" +
                " locationId = " + locationId +
                " city = " + city +
                " country    = " + country;
    }


}
