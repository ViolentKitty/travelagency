package com.bteam.TravelAgency.model;

public class Passenger extends User {

    private String firstName;
    private String lastName;
    private String phoneNumber;
    private int balance;

    public Passenger() {}

    public Passenger(Passenger passenger) {
        super(passenger);
        this.firstName = passenger.firstName;
        this.lastName = passenger.lastName;
        this.phoneNumber = passenger.phoneNumber;
        this.balance = passenger.getBalance();
    }

    public Passenger(String firstName, String lastName, String phoneNumber, int balance) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.balance = balance;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public String toString() {
        return "Passenger toString(): \n" +
                " firstName = " + firstName +
                " lastName = " + lastName +
                " phoneNumber = " + phoneNumber +
                " balance = " + balance;
    }

}
