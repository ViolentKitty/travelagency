package com.bteam.TravelAgency.model;

public class Flight {

    private int flightId;
    private String sourceLocation;
    private String destLocation;
    private String company;
    private int fare;

    public Flight() {}

    public int getFlightId() {
        return flightId;
    }

    public Flight setFlightId(int flightId) {
        this.flightId = flightId;
        return this;
    }

    public String getSourceLocation() {
        return sourceLocation;
    }

    public Flight setSourceLocation(String sourceLocation) {
        this.sourceLocation = sourceLocation;
        return this;
    }

    public String getDestLocation() {
        return destLocation;
    }

    public Flight setDestLocation(String destLocation) {
        this.destLocation = destLocation;
        return this;
    }

    public String getCompany() {
        return company;
    }

    public Flight setCompany(String company) {
        this.company = company;
        return this;
    }

    public int getFare() {
        return fare;
    }

    public Flight setFare(int fare) {
        this.fare = fare;
        return this;
    }

    public String toString() {
        return "Flight toString(): \n" +
                " flightId = " + flightId +
                " sourceLocation = " + sourceLocation +
                " destLocation = " + destLocation +
                " company = " + company +
                " fare = " + fare;
    }

}
