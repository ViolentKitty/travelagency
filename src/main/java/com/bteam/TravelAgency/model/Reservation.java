package com.bteam.TravelAgency.model;

public class Reservation {

    private int reservationId;
    private int passengerId;
    private String sourceLocation;
    private String destLocation;
    private int flightId;
    private String transportMode;
    private int groupSize;

    public int getReservationId() {
        return reservationId;
    }

    public Reservation setReservationId(int reservationId) {
        this.reservationId = reservationId;
        return this;
    }

    public int getPassengerId() {
        return passengerId;
    }

    public Reservation setPassengerId(int passengerId) {
        this.passengerId = passengerId;
        return this;
    }

    public String getSourceLocation() {
        return sourceLocation;
    }

    public Reservation setSourceLocation(String sourceLocation) {
        this.sourceLocation = sourceLocation;
        return this;
    }

    public String getDestLocation() {
        return destLocation;
    }

    public Reservation setDestLocation(String destLocation) {
        this.destLocation = destLocation;
        return this;
    }

    public int getFlightId() {
        return flightId;
    }

    public Reservation setFlightId(int flightId) {
        this.flightId = flightId;
        return this;
    }

    public String getTransportMode() {
        return transportMode;
    }

    public Reservation setTransportMode(String transportMode) {
        this.transportMode = transportMode;
        return this;
    }

    public int getGroupSize() {
        return groupSize;
    }

    public Reservation setGroupSize(int groupSize) {
        this.groupSize = groupSize;
        return this;
    }

    public String toString() {
        return "Reservation toString(): " +
                "reservationId = " + reservationId +
                "passengerId" + passengerId +
                "sourceLocationId" + sourceLocation +
                "destLocationId" + destLocation +
                "flightId" + flightId +
                "transportMode" + transportMode +
                "groupSize" + groupSize;
    }

}
