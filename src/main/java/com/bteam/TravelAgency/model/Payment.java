package com.bteam.TravelAgency.model;

public class Payment {

    private int paymentId;
    private int passengerId;
    private int reservationId;
    private long cardNumber;

    public int getPaymentId() {
        return paymentId;
    }

    public Payment setPaymentId(int paymentId) {
        this.paymentId = paymentId;
        return this;
    }

    public int getPassengerId() {
        return passengerId;
    }

    public Payment setPassengerId(int passengerId) {
        this.passengerId = passengerId;
        return this;
    }

    public int getReservationId() {
        return reservationId;
    }

    public Payment setReservationId(int reservationId) {
        this.reservationId = reservationId;
        return this;
    }

    public long getCardNumber() {
        return cardNumber;
    }

    public Payment setCardNumber(long cardNumber) {
        this.cardNumber = cardNumber;
        return this;
    }

    public String toString() {
        return "Payment toString(): " +
                "paymentId = " + paymentId +
                "passengerId" + passengerId +
                "reservationId" + reservationId +
                "cardNumber" + cardNumber;
    }

}
