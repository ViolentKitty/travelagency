package com.bteam.TravelAgency.model;

public class Accommodation {

    private int accommodationId;
    private String hotelName;
    private int cost;
    private String cityName;

    public Accommodation() {}

    public int getAccommodationId() {
        return accommodationId;
    }

    public Accommodation setAccommodationId(int accommodationId) {
        this.accommodationId = accommodationId;
        return this;
    }

    public String getHotelName() {
        return hotelName;
    }

    public Accommodation setHotelName(String hotelName) {
        this.hotelName = hotelName;
        return this;
    }

    public int getCost() {
        return cost;
    }

    public Accommodation setCost(int cost) {
        this.cost = cost;
        return this;
    }

    public String getCityName() {
        return cityName;
    }

    public Accommodation setCityName(String cityName) {
        this.cityName = cityName;
        return this;
    }

    public String toString() {
        return "Accommodation toString(): " +
                "accommodationId = " + accommodationId +
                "hotelName = " + hotelName +
                "cost = " + cost +
                "cityName = " + cityName;
    }

}
