package com.bteam.TravelAgency.model;

public class Review {

    private int paymentId;
    private int passengerId;
    private int reservationId;
    private String detailedReview;

    public int getPaymentId() {
        return paymentId;
    }

    public Review setPaymentId(int paymentId) {
        this.paymentId = paymentId;
        return this;
    }

    public int getPassengerId() {
        return passengerId;
    }

    public Review setPassengerId(int passengerId) {
        this.passengerId = passengerId;
        return this;
    }

    public int getReservationId() {
        return reservationId;
    }

    public Review setReservationId(int reservationId) {
        this.reservationId = reservationId;
        return this;
    }

    public String getDetailedReview() {
        return detailedReview;
    }

    public Review setDetailedReview(String detailedReview) {
        this.detailedReview = detailedReview;
        return this;
    }

    public String toString() {
        return "Payment toString(): " +
                "paymentId = " + paymentId +
                "passengerId" + passengerId +
                "reservationId" + reservationId +
                "detailedReview" + detailedReview;
    }

}
