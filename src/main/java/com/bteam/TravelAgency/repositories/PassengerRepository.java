package com.bteam.TravelAgency.repositories;

import com.bteam.TravelAgency.model.Passenger;
import com.bteam.TravelAgency.repositories.rowmappers.PassengerRowMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.List;

@Repository
public class PassengerRepository extends AbstractRepository<Passenger> {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public static final String TABLE_NAME = "Passenger";

    private static final Logger LOGGER = LoggerFactory.getLogger(PassengerRepository.class);

    public int updateUserBalance(String email, int newBalance) {
        Passenger passenger = findByEmailId(email);
        int currentBalance = passenger.getBalance();
        newBalance += currentBalance;

        String sql = "UPDATE Passenger SET balance = ? WHERE email = ?";

        int rowsAffected = jdbcTemplate.update(sql, new Object[] {newBalance, email});

        if (rowsAffected == 0) {
            LOGGER.error("Something went wrong while trying to update Passenger table");
        } else {
            LOGGER.error("Update to Passenger was successful");
        }
        return rowsAffected;
    }

    public int clearUserBalance(String email) {

        String sql = "UPDATE Passenger SET balance = 0 WHERE email = ?";

        int rowsAffected = jdbcTemplate.update(sql, new Object[] {email});

        if (rowsAffected == 0) {
            LOGGER.error("Something went wrong while trying to update Passenger table");
        } else {
            LOGGER.error("Update to Passenger was successful");
        }
        return rowsAffected;
    }

    public Passenger findByEmailId(String email) {
        String sql = "SELECT * FROM " + TABLE_NAME + " WHERE email = ?";

        List<Passenger> results = jdbcTemplate.query(
                sql,
                new Object[] { email },
                new PassengerRowMapper());

        System.out.println("SQL QUERY: " + sql);

        if(results.isEmpty()) {
            return null;
        } else if(results.size() > 1) {
            LOGGER.error("Passenger list returned for email is " + results.size() + "!");
            return null;
        } else {
            return results.get(0);
        }
    }

    @Override
    public Number save(Passenger passenger) {
        Passenger p = findByEmailId(passenger.getEmailId());
        if(p != null) {
            return null;
        }
        SimpleJdbcInsert jdbcInsert = new SimpleJdbcInsert(jdbcTemplate);
        jdbcInsert.setTableName("Passenger");
        jdbcInsert.usingGeneratedKeyColumns(PassengerRowMapper.PassengerAttribute.id.toString());

        final HashMap<String, Object> params = new HashMap<>();
        params.put(PassengerRowMapper.PassengerAttribute.firstName.toString(), passenger.getFirstName());
        params.put(PassengerRowMapper.PassengerAttribute.lastName.toString(), passenger.getLastName());
        params.put(PassengerRowMapper.PassengerAttribute.phoneNumber.toString(), passenger.getPhoneNumber());
        params.put(PassengerRowMapper.PassengerAttribute.email.toString(), passenger.getEmailId());
        params.put(PassengerRowMapper.PassengerAttribute.password.toString(), passenger.getPassword());

        final Number key = jdbcInsert.executeAndReturnKey(params);
        passenger.setId(key.intValue());

        return key;
    }

    @Override
    String getTableName() {
        return TABLE_NAME;
    }

    @Override
    String getIdAttributeName() {
        return PassengerRowMapper.PassengerAttribute.id.toString();
    }

    @Override
    RowMapper<Passenger> getRowMapper() {
        return new PassengerRowMapper();
    }

}

