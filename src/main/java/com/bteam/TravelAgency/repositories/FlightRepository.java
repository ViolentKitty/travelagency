package com.bteam.TravelAgency.repositories;

import com.bteam.TravelAgency.model.Flight;
import com.bteam.TravelAgency.model.Location;
import com.bteam.TravelAgency.repositories.rowmappers.FlightRowMapper;
import com.bteam.TravelAgency.repositories.rowmappers.LocationRowMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public class FlightRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private static final Logger LOGGER = LoggerFactory.getLogger(FlightRepository.class);

    public Flight findFlightById(int flightId) {
        String sql = "SELECT * FROM Flight WHERE flightId = ?";

        List<Flight> results = jdbcTemplate.query(sql, new Object[] {flightId}, new FlightRowMapper());
        fixFlightLocations(results);

        // error checking
//        Flight flight = (results.isEmpty() || results.size() > 1) ? null : results.get(0);
//        return flight;
        if (results.isEmpty()) {
            return null;
        }
        if (results.size() > 1) {
            System.out.println("TOO MANY ITEMS");
            return null;
        }
        return results.get(0);
    }

    public List<Flight> findAll() {
        String sql = "SELECT * FROM Flight";

        List<Flight> results = jdbcTemplate.query(sql, new Object[] { }, new FlightRowMapper());
        fixFlightLocations(results);

        return results;
    }
    public List<Flight> findAllGivenCities(String source, String dest) {


        String sql = "SELECT * FROM Flight WHERE sourceLocation = ? AND destLocation = ?";

        List<Flight> results = jdbcTemplate.query(sql, new Object[] {source, dest}, new FlightRowMapper());
        fixFlightLocations(results);

        return results;
    }

    public List<Flight> fixFlightLocations(List<Flight> result) {
        String sql = "SELECT * FROM Location";
        System.out.println("fixFlightLocation");
        List<Location> locationResult = jdbcTemplate.query(sql, new Object[] { }, new LocationRowMapper());
        System.out.println(locationResult.size());
        if (result.isEmpty()) {
            return result;
        }

        for (int i = 0; i < result.size(); i++) {
            String actualSource = locationResult.get(Integer.parseInt(result.get(i).getSourceLocation()) - 1).getCity();
            String actualDest = locationResult.get(Integer.parseInt(result.get(i).getDestLocation()) - 1).getCity();
            result.get(i).setSourceLocation(actualSource);
            result.get(i).setDestLocation(actualDest);
        }
        return result;
    }

}
