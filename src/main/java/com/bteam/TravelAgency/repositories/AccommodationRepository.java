package com.bteam.TravelAgency.repositories;

import com.bteam.TravelAgency.model.Accommodation;
import com.bteam.TravelAgency.repositories.rowmappers.AccommodationRowMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public class AccommodationRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    private static final Logger LOGGER = LoggerFactory.getLogger(AccommodationRepository.class);

    public List<Accommodation> findAccommodationsByCity(String cityName) {
        String sql = "SELECT * FROM Accommodation WHERE cityName = ?";

        List<Accommodation> results = jdbcTemplate.query(sql, new Object[] {cityName}, new AccommodationRowMapper());

        return results;
    }

    public Accommodation findAccommodationById(int accommodationId) {
        String sql = "SELECT * FROM Accommodation WHERE accommodationId = ?";

        List<Accommodation> results = jdbcTemplate.query(sql, new Object[] {accommodationId}, new AccommodationRowMapper());

        if (results.isEmpty()) {
            return null;
        }

        return results.get(0);
    }


}
