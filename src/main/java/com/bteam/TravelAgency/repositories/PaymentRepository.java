package com.bteam.TravelAgency.repositories;

import com.bteam.TravelAgency.model.Payment;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class PaymentRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(PaymentRepository.class);

    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private PassengerRepository passengerRepository;

    public int savePayment(String loggedEmail, String cardNumber, String paymentType) {

        int passengerId = passengerRepository.findByEmailId(loggedEmail).getId();

        String sql = "INSERT INTO Payment(paymentType, cardNumber, passengerId)" +
                "VALUES(?, ?, ?)";

        int rowsAffected = jdbcTemplate.update(sql, new Object[] {paymentType, cardNumber, passengerId});

        if (rowsAffected == 0) {
            LOGGER.error("insertion into Payment failed, 0 rows were affected");
        } else {
            LOGGER.error("insertion into Payment success");
        }
        
        return rowsAffected;

    }

}
