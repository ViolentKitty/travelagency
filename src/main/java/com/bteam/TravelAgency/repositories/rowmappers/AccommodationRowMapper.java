package com.bteam.TravelAgency.repositories.rowmappers;

import com.bteam.TravelAgency.model.Accommodation;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.bteam.TravelAgency.repositories.rowmappers.AccommodationRowMapper.AccommodationAttribute.*;

public class AccommodationRowMapper implements RowMapper<Accommodation> {

    @Override
    public Accommodation mapRow(ResultSet resultSet, int i) throws SQLException {
        Accommodation accommodation = new Accommodation();

        accommodation.setAccommodationId(resultSet.getInt(accommodationId.toString()));
        accommodation.setHotelName(resultSet.getString(hotelName.toString()));
        accommodation.setCost(resultSet.getInt(cost.toString()));
        accommodation.setCityName(resultSet.getString(cityName.toString()));

        return accommodation;
    }

    public enum AccommodationAttribute implements Attributes {
        accommodationId ("accommodationId"),
        hotelName ("hotelName"),
        cost("cost"),
        cityName("cityName");

        private final String name;

        private static List<String> attributes;

        private AccommodationAttribute(String s) {
            name = s;
        }

        public boolean equalsName(String otherName) {
            return name.equals(otherName);
        }

        public String toString() {
            return this.name;
        }

        @Override
        public List<String> getAttributes() {
            if(attributes != null) {
                return attributes;
            }
            attributes = new ArrayList<>();
            for(AccommodationAttribute attribute : AccommodationAttribute.values()) {
                attributes.add(attribute.toString());
            }
            return attributes;
        }
    }

}


