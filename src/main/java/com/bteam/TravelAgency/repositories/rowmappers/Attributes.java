package com.bteam.TravelAgency.repositories.rowmappers;

import java.util.List;

public interface Attributes {
    List<String> getAttributes();
}

