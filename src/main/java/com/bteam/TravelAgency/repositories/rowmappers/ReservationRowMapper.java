package com.bteam.TravelAgency.repositories.rowmappers;

import com.bteam.TravelAgency.model.Reservation;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.bteam.TravelAgency.repositories.rowmappers.ReservationRowMapper.ReservationAttribute.*;

public class ReservationRowMapper implements RowMapper<Reservation> {

    @Override
    public Reservation mapRow(ResultSet resultSet, int i) throws SQLException {
        Reservation reservation = new Reservation();

        reservation.setReservationId(resultSet.getInt(reservationId.toString()));
        reservation.setPassengerId(resultSet.getInt(passengerId.toString()));
        reservation.setSourceLocation(resultSet.getString(srcLocation.toString()));
        reservation.setDestLocation(resultSet.getString(destLocation.toString()));
        reservation.setFlightId(resultSet.getInt(flightId.toString()));
        reservation.setTransportMode(resultSet.getString(transportMode.toString()));
        reservation.setGroupSize(resultSet.getInt(groupSize.toString()));

        return reservation;
    }

    public enum ReservationAttribute implements Attributes {
        reservationId ("reservationId"),
        passengerId ("passengerId"),
        srcLocation ("sourceLocation"),
        destLocation ("destLocation"),
        flightId ("flightId"),
        transportMode ("transportMode"),
        groupSize ("groupSize");

        private final String name;

        private static List<String> attributes;

        private ReservationAttribute(String s) {
            name = s;
        }

        public boolean equalsName(String otherName) {
            return name.equals(otherName);
        }

        public String toString() {
            return this.name;
        }

        @Override
        public List<String> getAttributes() {
            if(attributes != null) {
                return attributes;
            }
            attributes = new ArrayList<>();
            for(ReservationAttribute attribute : ReservationAttribute.values()) {
                attributes.add(attribute.toString());
            }
            return attributes;
        }
    }

}



