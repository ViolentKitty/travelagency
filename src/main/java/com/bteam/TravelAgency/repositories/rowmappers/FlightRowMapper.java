package com.bteam.TravelAgency.repositories.rowmappers;

import com.bteam.TravelAgency.model.Flight;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.bteam.TravelAgency.repositories.rowmappers.FlightRowMapper.FlightAttribute.*;

public class FlightRowMapper implements RowMapper<Flight> {

    @Override
    public Flight mapRow(ResultSet resultSet, int i) throws SQLException {
        Flight flight = new Flight();

        flight.setFlightId(resultSet.getInt(flightId.toString()));
        flight.setSourceLocation(resultSet.getString(sourceLocation.toString()));
        flight.setDestLocation(resultSet.getString(destLocation.toString()));
        flight.setCompany(resultSet.getString(company.toString()));
        flight.setFare(resultSet.getInt(fare.toString()));

        return flight;
    }

    public enum FlightAttribute implements Attributes {
        flightId ("flightId"),
        sourceLocation ("sourceLocation"),
        destLocation("destLocation"),
        company("company"),
        fare("fare");

        private final String name;

        private static List<String> attributes;

        private FlightAttribute(String s) {
            name = s;
        }

        public boolean equalsName(String otherName) {
            return name.equals(otherName);
        }

        public String toString() {
            return this.name;
        }

        @Override
        public List<String> getAttributes() {
            if(attributes != null) {
                return attributes;
            }
            attributes = new ArrayList<>();
            for(FlightAttribute attribute : FlightAttribute.values()) {
                attributes.add(attribute.toString());
            }
            return attributes;
        }
    }

}


