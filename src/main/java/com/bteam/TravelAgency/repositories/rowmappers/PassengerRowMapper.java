package com.bteam.TravelAgency.repositories.rowmappers;

import com.bteam.TravelAgency.model.Passenger;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.bteam.TravelAgency.repositories.rowmappers.PassengerRowMapper.PassengerAttribute.*;

public class PassengerRowMapper implements RowMapper<Passenger> {

    @Override
    public Passenger mapRow(ResultSet resultSet, int i) throws SQLException {
        Passenger passenger = new Passenger();

        passenger.setId(resultSet.getInt(id.toString()))
                .setEmailId(resultSet.getString(email.toString()))
                .setPassword(resultSet.getString(password.toString()));
        passenger.setFirstName(resultSet.getString(firstName.toString()));
        passenger.setLastName(resultSet.getString(lastName.toString()));
        passenger.setPhoneNumber(resultSet.getString(phoneNumber.toString()));
        passenger.setBalance(resultSet.getInt(balance.toString()));

        return passenger;
    }

    public enum PassengerAttribute implements Attributes {
        id ("passengerId"),
        email("email"),
        password("password"),
        firstName("firstName"),
        lastName("lastName"),
        phoneNumber("phoneNumber"),
        balance("balance");

        private final String name;

        private static List<String> attributes;

        private PassengerAttribute(String s) {
            name = s;
        }

        public boolean equalsName(String otherName) {
            return name.equals(otherName);
        }

        public String toString() {
            return this.name;
        }

        @Override
        public List<String> getAttributes() {
            if(attributes != null) {
                return attributes;
            }
            attributes = new ArrayList<>();
            for(PassengerAttribute attribute : PassengerAttribute.values()) {
                attributes.add(attribute.toString());
            }
            return attributes;
        }
    }

}


