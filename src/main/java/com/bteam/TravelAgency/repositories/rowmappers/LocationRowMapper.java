package com.bteam.TravelAgency.repositories.rowmappers;

import com.bteam.TravelAgency.model.Location;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.bteam.TravelAgency.repositories.rowmappers.LocationRowMapper.LocationAttribute.*;

public class LocationRowMapper implements RowMapper<Location> {

    @Override
    public Location mapRow(ResultSet resultSet, int i) throws SQLException {
        Location location = new Location();

        location.setLocationId(resultSet.getInt(locationId.toString()));
        location.setCity(resultSet.getString(city.toString()));
        location.setCountry(resultSet.getString(country.toString()));

        return location;
    }

    public enum LocationAttribute implements Attributes {
        locationId ("locationId"),
        city ("city"),
        country("country");

        private final String name;

        private static List<String> attributes;

        private LocationAttribute(String s) {
            name = s;
        }

        public boolean equalsName(String otherName) {
            return name.equals(otherName);
        }

        public String toString() {
            return this.name;
        }

        @Override
        public List<String> getAttributes() {
            if(attributes != null) {
                return attributes;
            }
            attributes = new ArrayList<>();
            for(LocationAttribute attribute : LocationAttribute.values()) {
                attributes.add(attribute.toString());
            }
            return attributes;
        }
    }

}



