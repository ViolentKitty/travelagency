package com.bteam.TravelAgency.repositories;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;

import java.sql.Types;
import java.util.List;

public abstract class AbstractRepository<E> {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    public E findById(int id) {
        String sql = "SELECT * FROM " + getTableName() + " WHERE " + getIdAttributeName() + " = ?";

        List<E> results = jdbcTemplate.query(
                sql,
                new Object[] { id },
                getRowMapper());

        System.out.println("SQL QUERY: " + sql);

        if(results.isEmpty()) {
            return null;
        } else if(results.size() > 1) {
            System.out.println(getTableName() + " list returned for id " + getIdAttributeName() + " is " + results.size() + "!");
            return null;
        } else {
            return results.get(0);
        }
    }

    public boolean delete(int id) {
        if(findById(id) == null) {
            return false;
        }
        String sql = "DELETE FROM " + getTableName() + " WHERE " + getIdAttributeName() + " = ?";

        Object[] params = { id };
        int[] types = {Types.BIGINT };

        int num_rows_affected = jdbcTemplate.update(sql, params, types);

        System.out.println("SQL QUERY: " + sql);

        return num_rows_affected == 1;
    }

    public abstract Number save(E obj);
    abstract String getTableName();
    abstract String getIdAttributeName();
    abstract RowMapper<E> getRowMapper();
}

