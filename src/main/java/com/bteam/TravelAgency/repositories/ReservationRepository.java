package com.bteam.TravelAgency.repositories;

import com.bteam.TravelAgency.model.Flight;
import com.bteam.TravelAgency.model.Location;
import com.bteam.TravelAgency.model.Passenger;
import com.bteam.TravelAgency.model.Reservation;
import com.bteam.TravelAgency.repositories.rowmappers.FlightRowMapper;
import com.bteam.TravelAgency.repositories.rowmappers.LocationRowMapper;
import com.bteam.TravelAgency.repositories.rowmappers.ReservationRowMapper;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ReservationRepository {

    @Autowired
    private JdbcTemplate jdbcTemplate;
    @Autowired
    private PassengerRepository passengerRepository;
    @Autowired
    private FlightRepository flightRepository;
    private static final org.slf4j.Logger LOGGER = LoggerFactory.getLogger(ReservationRepository.class);

    public List<Reservation> findReservationsForUser(String email) {
        Passenger passenger = passengerRepository.findByEmailId(email);
        int passengerId = passenger.getId();

        String sql = "SELECT * FROM Reservation WHERE passengerId = ?";

        List<Reservation> results = jdbcTemplate.query(sql, new Object[] {passengerId}, new ReservationRowMapper());

        // Fix location names in Reservation objects
        List<Location> locationResults = findAllLocation();
        for (int i = 0; i < results.size(); i++) {
            results.get(i).setSourceLocation(locationResults.get(Integer.parseInt(results.get(i).getSourceLocation()) - 1).getCity());
            results.get(i).setDestLocation(locationResults.get(Integer.parseInt(results.get(i).getDestLocation()) - 1).getCity());
        }

        return results;
    }

    public Location findLocationByCity(String city) {

        String sql = "SELECT * FROM Location WHERE city = ?";

        List<Location> results = jdbcTemplate.query(sql, new Object[]{city}, new LocationRowMapper());

        if (results.isEmpty()) {
            return null;
        } else if (results.size() >= 2) {
            System.out.println("query returned more than 1 result from Location table with city = " + city);
            return null;
        } else {
            return results.get(0);
        }
    }

    public List<Location> findAllLocation() {
        String sql = "SELECT * FROM Location";
        List<Location> results = jdbcTemplate.query(sql, new Object[] { }, new LocationRowMapper());

        return results;
    }

    public List<Flight> findAllFlight() {
        String sql = "SELECT * FROM Flight";
        List<Flight> results = jdbcTemplate.query(sql, new Object[] { }, new FlightRowMapper());

        return results;
    }

    public String locationMapping_nameToId(String name) {
        String locationId;
        List<Location> results = findAllLocation();

        for (int i = 0; i < results.size(); i++) {
            if (results.get(i).getCity().equals(name)) {
                return Integer.toString(i + 1);
            }
        }

        return null;
    }

    public int saveReservation(String loggedEmail, Location sourceLocation, Location destLocation, String mode, String flightId, String groupSize) {

        int passengerId = passengerRepository.findByEmailId(loggedEmail).getId();
        int srcLocationId = sourceLocation.getLocationId();
        int destLocationId = destLocation.getLocationId();
        int flightId_int = Integer.parseInt(flightId);
        int groupSize_int = Integer.parseInt(groupSize);

        String sql = "INSERT INTO Reservation(passengerId, sourceLocation, destLocation, flightId, groupSize)" +
                     "VALUES(?, ?, ?, ?, ?)";

        int rowsAffected = jdbcTemplate.update(sql, new Object[] {passengerId, srcLocationId, destLocationId, flightId_int, groupSize_int});

        if (rowsAffected == 0) {
            LOGGER.error("insertion into Reservation failed, 0 rows were affected");
        } else {
            LOGGER.error("insertion into Reservation success");
        }
        return rowsAffected;

    }

}
