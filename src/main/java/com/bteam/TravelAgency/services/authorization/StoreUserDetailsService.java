package com.bteam.TravelAgency.services.authorization;

import com.bteam.TravelAgency.model.Passenger;
import com.bteam.TravelAgency.repositories.PassengerRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class StoreUserDetailsService implements UserDetailsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(StoreUserDetailsService.class);

    @Autowired
    private PassengerRepository passengerRepository;

    @Override
    @Transactional(readOnly = true)
    public UserDetails loadUserByUsername(String emailId) throws UsernameNotFoundException {
        Passenger passenger = passengerRepository.findByEmailId(emailId);

        if(passenger == null) {
            LOGGER.error("User not found!");
            System.out.println("User not found!");
            throw new UsernameNotFoundException("User not found with the email id " + emailId);
        } else {
            LOGGER.error(passenger.getEmailId());
            LOGGER.error(passenger.getPassword());
            PassengerDetails pd = new PassengerDetails(passenger);
            LOGGER.error(pd.getPassword());
            return new PassengerDetails(passenger);
        }
    }

}
