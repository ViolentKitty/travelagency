package com.bteam.TravelAgency.services.authorization;

public enum Role {
    ROLE_CUSTOMER(1);
    private final int value;
    private Role(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
