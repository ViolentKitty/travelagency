package com.bteam.TravelAgency.services.authorization;

import com.bteam.TravelAgency.model.Passenger;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.ArrayList;
import java.util.Collection;

public class PassengerDetails extends Passenger implements UserDetails {

    public PassengerDetails(Passenger passenger) { super(passenger); }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        final Collection<GrantedAuthority> grantedAuthorities = new ArrayList<>();
        grantedAuthorities.add(new SimpleGrantedAuthority(Role.ROLE_CUSTOMER.toString()));
        return grantedAuthorities;
    }

    @Override
    public String getPassword() { return super.getPassword(); }

    @Override
    public String getUsername() { return super.getEmailId(); }

    @Override
    public boolean isAccountNonExpired() { return true; }

    @Override
    public boolean isAccountNonLocked() { return true; }

    @Override
    public boolean isCredentialsNonExpired() { return true; }

    @Override
    public boolean isEnabled() { return true; }

}
