package com.bteam.TravelAgency.services;

import com.bteam.TravelAgency.model.Passenger;
import com.bteam.TravelAgency.repositories.PassengerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PassengerService {

    @Autowired
    private PassengerRepository passengerRepository;

    public Passenger save(Passenger passenger) {
        if (passengerRepository.save(passenger) ==  null) {
            return null;
        }
        return passenger;
    }

}
